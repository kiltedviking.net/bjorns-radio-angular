# kiltedviking.net/bjorns-radio-angular

This is a first go at creating Kilted Viking's example project Bjorn's Radio - a fictitious radio station run by it's members (all called Björn ;-)).

(Please note that I'm currently learning AngularJS, v. 1.x, as well as Git and GitLab, so the project isn't all that exciting yet..)

Björn G. D. Persson
Kilted Viking
https://gitlab.com/kiltedviking.net/